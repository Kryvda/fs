import java.io.*;
//import java.util.Scanner;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.file.StandardOpenOption;

class Directory {
    static final int DIR_MAX_ELEMS = 20;

    static String path;

    public static void create(String pathToCreate, String name) {
        path = pathToCreate + '\\' + name;
        File dir = new File(pathToCreate);


        File file = new File(path);

        int numberOfSubfolders = 0;
        File listDir[] = dir.listFiles();
        for (int i = 0; i < listDir.length; i++) {
            if (listDir[i].isDirectory()) {
                numberOfSubfolders++;
            }
        }

        //Creating a File object

        //Creating the directory
        ;
        if (!(numberOfSubfolders < DIR_MAX_ELEMS)) {
            System.out.println("Sorry couldn’t create specified directory");
            return;
        }
        if (file.mkdir()) {
            System.out.println("Directory created successfully");
        } else {
            System.out.println("Sorry couldn’t create specified directory");
        }

    }

    public static void delete(String path) {
        File dir = new File(path);
        if (!(dir.exists())) {
            System.out.println("Не існує");
            return;
        }
        if (!(dir.isDirectory())) {
            System.out.println("Не директорія");
            return;
        }
        dir.delete();
        System.out.println("Директорія видалена.");
    }

    public static void content(String path) {
        File dir = new File(path);
        if (dir.exists()) {
            for (int i = 0; i < dir.listFiles().length; i++) {
                System.out.println(dir.listFiles()[i].getName());
            }
            return;
        }

    }

    public static void move(String path, String newPath) throws IOException, ClassNotFoundException {
        if (new File(path).isDirectory()) {
            {
                File dir[] = new File(path).listFiles();
                File newDir = new File(newPath);
                create(newDir.getPath(), new File(path).getName());
                for (int i = 0; i < dir.length; i++) {
                    //newDir = new File

                    move(dir[i].getPath(), newDir.getPath() + '\\' + new File(path).getName());
                    dir[i].delete();
                }
                return;
            }
        }

        Object data = BinaryFile.read(path);


        BinaryFile.create(newPath + '\\' + new File(path).getName(), data);
        //System.out.println(new File(path).delete());
        BinaryFile.delete(path);
    }


}

class BinaryFile<T> {
    public static void create(String path, Object obj) throws IOException {
        FileOutputStream fos = new FileOutputStream(path);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(obj);
        fos.close();
        oos.close();
    }

    public static Object read(String path) throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(path);
        ObjectInputStream ois = new ObjectInputStream(fis);
        Object val = ois.readObject();
        fis.close();
        ois.close();
        return val;
    }

    public static void delete(String path) {
        File file = new File(path);
        if (file.exists()) {
            file.delete();
        }

    }

    public static void move(String path, String newPath) throws IOException, ClassNotFoundException {
        File file = new File(path);
        if (!file.exists() || file.isDirectory() || !new File(newPath).isDirectory()) {
            System.out.println("file does not exist");
            return;
        }
        newPath += '\\' + file.getName();
        Object obj = read(path);
        File newFile = new File(newPath);
        newFile.createNewFile();
        create(newPath, obj);
        file.delete();
    }
}

class TextFile {
    public static void create(String path, String info) throws IOException {
        if (!check(path)) {
            System.out.println('n');
            return;
        }
        if (new File(path).exists()) {
        } else {
            new File(path).createNewFile();
        }
        FileWriter writer = new FileWriter(path, true);
        PrintWriter printer = new PrintWriter(writer);
        printer.println(info);
        printer.close();
        writer.close();
    }

    public static void move(String path, String newPath) throws IOException, ClassNotFoundException {
        if (!check(path)) {
            System.out.println("error");
            return;
        }
        File file = new File(path);
        if (!file.exists() || file.isDirectory() || !new File(newPath).isDirectory()) {
            System.out.println("error");
            return;
        }
        newPath += '\\' + file.getName();
        String str = read(path);
        File newFile = new File(newPath);
        newFile.createNewFile();
        create(newPath, str);
        boolean b = file.delete();
    }

    public static void append(String path, String str) throws IOException {
        if (!check(path)) {
            System.out.println('n');
            return;
        }
        if (new File(path).exists()) {
        } else {
            System.out.println("No file exists");
        }
        FileWriter writer = new FileWriter(path, true);
        PrintWriter printer = new PrintWriter(writer);
        printer.println(str);
        printer.close();
        writer.close();
    }

    public static boolean check(String txt) {
        if (txt.charAt(txt.length() - 1) == 't') {
            if (txt.charAt(txt.length() - 2) == 'x') {
                if (txt.charAt(txt.length() - 3) == 't') {
                    if (txt.charAt(txt.length() - 4) == '.') {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static String read(String path) throws IOException {
        FileReader reader = new FileReader(path);
        BufferedReader bRreader = new BufferedReader(reader);
        String res = "";
        String temp = bRreader.readLine();
        while (temp != null) {
            res += temp + "\n";
            temp = bRreader.readLine();
        }
        bRreader.close();
        reader.close();
        return res;
    }

}

class Buffer {
    final int MAX_BUF_FILE_SIZE = 25;
    String path;
    int count = 0;

    public Buffer(String Path) {
        this.path = Path;
        File buffer = new File(path);
        try {
            buffer.createNewFile();
        } catch (IOException e) {
            System.out.println(e.getMessage());

        }

    }

    public boolean appendData(Object obj) throws IOException {


        if (count < MAX_BUF_FILE_SIZE) {
            FileOutputStream fos = new FileOutputStream(path, true);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(obj);
            count++;
            oos.close();
            fos.close();
            return true;
        } else {

            return false;
        }
    }

    public boolean getData() throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(path);
        ObjectInputStream ois = new ObjectInputStream(fis);
        Object val = ois.readObject();
        Object val2 = ois.read();
        fis.close();
        ois.close();
        return true;
    }
}

public class Main {
    static Directory dir = new Directory();
    static int i = 0;
    static TextFile txt = new TextFile();

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        while (i < 10) {
            //dir.create("C:\\Users\\тигор\\IdeaProjects\\_1\\src", "dir" + i);
            i++;
        }

        BinaryFile.create("C:\\Users\\тигор\\IdeaProjects\\_1\\dir1\\gh2", i);
        //dir.delete("C:\\Users\\тигор\\IdeaProjects\\_1\\dir0");
        dir.content("C:\\Users\\тигор\\IdeaProjects\\_1");
        dir.move("C:\\Users\\тигор\\IdeaProjects\\_1\\dir1", "C:\\Users\\тигор\\IdeaProjects\\_1\\dir0");
        // int c = (Integer)BinaryFile.read("C:\\Users\\тигор\\IdeaProjects\\_1\\dir0\\gh2");
        //System.out.println(c);
        txt.create("dir0\\file.txt", "гииииии");
        txt.append("file.txt", "нова інфа");
        BinaryFile.move("C:\\Users\\тигор\\IdeaProjects\\_1\\dir0\\dir1\\gh2",
                "C:\\Users\\тигор\\IdeaProjects\\_1\\dir0");
        TextFile.move("dir0\\file.txt", "dir0\\dir1");
        Buffer buff = new Buffer("buffer");
        buff.appendData(i);
        i = 5;
        buff.appendData(i);
       buff.getData();
    }
}